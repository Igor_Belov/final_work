class CreateInstitutions < ActiveRecord::Migration[5.2]
  def change
    create_table :institutions do |t|
      t.string :title
      t.references :category, foreign_key: true
      t.text :description
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end

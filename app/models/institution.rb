class Institution < ApplicationRecord
  belongs_to :category
  belongs_to :user

  has_many :pictures
  has_many :reviews
end

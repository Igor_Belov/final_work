class User < ApplicationRecord

	has_many :reviews
	has_many :institutions
	has_many :pictures
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end

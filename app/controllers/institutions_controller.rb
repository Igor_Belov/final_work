class InstitutionsController < ApplicationController
  def index
  	@institutions = Institution.all
  end

  def new
  	@institution = Institution.new
  end

  def create
  	@institution = current_user.institutions.build(institution_params)
    if @institution.save
      redirect_to institutions_path
    else
      render 'edit'
    end
  end

  def destroy

    @institution = Institution.find(params[:id])
    @institution.delete

    redirect_to institutions_path
  
  end

  private

  def institution_params
    params.require(:institution).permit(:title, :description, :category_id, :user_id)
  end
end

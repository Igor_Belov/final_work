class CategoriesController < ApplicationController
  def index
    @categories = Category.all
  end

  def new
    @category = Category.new
  end

  def show
   
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to categories_path
    else
      render 'edit'
    end
  end

  def destroy

    @category = Category.find(params[:id])
    @category.delete

    redirect_to categories_path
  
  end

  private
  def category_params
    params.require(:category).permit(:title)
  end

  
end

require 'test_helper'

class CategoryesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get categoryes_index_url
    assert_response :success
  end

  test "should get new" do
    get categoryes_new_url
    assert_response :success
  end

  test "should get show" do
    get categoryes_show_url
    assert_response :success
  end

  test "should get create" do
    get categoryes_create_url
    assert_response :success
  end

  test "should get delete" do
    get categoryes_delete_url
    assert_response :success
  end

end

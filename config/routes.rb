Rails.application.routes.draw do
  root 'institutions#index'
  resources :categories, :institutions, :pictures, :reviews
  ActiveAdmin.routes(self)
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
